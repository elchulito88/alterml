FROM python:3.6.4-slim

## make a local directory
RUN mkdir /opt/alterML

# set "alterML" as the working directory from which CMD, RUN, ADD references
WORKDIR /opt/alterML

# copy the local requirements.txt to the /alterML directory
ADD requirements.txt .

# pip install the local requirements.txt
RUN pip install -r requirements.txt

# now copy all the files in this directory to /alterML directory
ADD . .
