from flashtext import KeywordProcessor
from csv import reader
import argparse
import glob


class FileScrapper:

    """
    Providing two paramters
    """

    def __init__(self, f_name, key_phrase):
        self.file = f_name
        self.key_phrase = key_phrase

    def concatenate(self):
        """
        Concatenates all the text files together into one
        :return:
        """
        with open('./' + self.file,  'w') as outfile:
            for fname in glob.glob("*.txt"):
                with open(fname) as infile:
                    for line in infile:
                        outfile.write(line)
        outfile.close()
        return

    def load_csv(self):
        data = list()
        keyword_processor = KeywordProcessor()
        keyword_processor.add_keyword(self.key_phrase)
        with open(self.file, 'r') as files:
            csv_reader = reader(files)
            for row_number, row in enumerate(csv_reader):
                if not row:
                    continue
                keywords_found = keyword_processor.extract_keywords(str(row))  # add span_info=True for index values
                if keywords_found:
                    data.append([row_number + 1, keywords_found[0]])
        return data

    def write_to_csv(self):
        thelist = self.load_csv()
        thefile = open('../datasets/results.txt', 'w')
        for item in thelist:
            thefile.write("%s\n" % str(item))
        thefile.close()
        return


if __name__ == "__main__":
    """
    This script will be used in the future to concatenate text files together
    and then use keywords to search for certain key phrases.

    Ex. terminal input
    >>> python scraper.py -f "final.txt" -s keywords

    """

    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--name", type=str, help="Enter the filename")
    parser.add_argument("-s", "--scrapper", type=str, help="Enter the keyword to scrape.")
    args = parser.parse_args()
    f = args.name
    key = args.scrapper

    scraper = FileScrapper(f, key)

    # scraper.concatenate()

    looplist = scraper.load_csv()
    # print(looplist)
    # for row_line in looplist:
    #     print (row_line)

    looplist.write_to_csv()

    # Examples of terminal input
    # python "parsing/text_scraper.py" -f "datasets/final.txt" -s objective
