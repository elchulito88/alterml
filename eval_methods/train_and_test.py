"""
Example of Splitting a Contrived Dataset into Train and Test
"""
from random import randrange
from random import seed


class TrainTest:
    split = 0.60
    k_folds = 4

    def __init__(self, dataset, cross_val=None):
        self.dataset = dataset
        self.cv = cross_val

    def train_test_split(self):
        """
        Split a dataset into a train and test set
        :param dataset:
        :param split:
        :return:
        """
        dataset = self.dataset
        train = list()
        train_size = self.split * len(dataset)
        dataset_copy = list(dataset)
        while len(train) < train_size:
            index = randrange(len(dataset_copy))
            train.append(dataset_copy.pop(index))
        return train, dataset_copy

    def cross_validation_split(self):
        """
        Split a dataset into k folds
        :param folds:
        :return:
        """

        if self.cv == None:
            dataset = self.dataset
        else:
            dataset = self.train_test_split()[0]

        folds = self.k_folds
        dataset_split = list()
        dataset_copy = list(dataset)
        fold_size = int(len(dataset) / folds)
        for i in range(folds):
            fold = list()
            while len(fold) < fold_size:
                index = randrange(len(dataset_copy))
                fold.append(dataset_copy.pop(index))
            dataset_split.append(fold)
        return dataset_split

    @classmethod
    def set_test_percent(cls, percent_amount):
        """
        Set the test percentage. It's 0.60 by default.
        :param percent_amount:
        :return:
        """
        cls.split = percent_amount

    @classmethod
    def set_kfolds(cls, kfolds_amount):
        """
        Set the number of kfolds
        :param kfolds_amount:
        :return:
        """
        cls.k_folds = kfolds_amount


# if __name__ == "__main__":
#     # test train/test split
#     seed(2)
#     dataset = [[1], [2], [3], [4], [5], [6], [7], [8], [9], [10]]
#
#     data_result = TrainTest(dataset, "Y")
#     # This step allows us to change the training/test data split.
#     TrainTest.set_test_percent(0.50)
#     print(TrainTest.k_folds)
#     TrainTest.set_kfolds(5)
#     print(TrainTest.k_folds)
#
#     train, test = data_result.train_test_split()
#     print(data_result.cross_validation_split())
#
#     print(train)
#     print(test)
