"""
This script is a class that scores the accuracy of a model using different formulas
"""
from math import sqrt


class Accuracy:
    """
    This class will call different formulas to verify accuracy
    """
    def __init__(self, actual, predicted):
        self.actual = actual
        self.predicted = predicted

    def accuracy_metric(self):
        """
        Calculate accuracy percentage between two lists
        """
        correct = 0

        for i in range(len(self.actual)):
            if self.actual[i] == self.predicted[i]:
                correct += 1
        return correct / float(len(self.actual)) * 100.0

    def confusion_matrix(self):
        """
        Calculate a confusion matrix
        :return:
        """
        unique = set(self.actual)

        matrix = [list() for x in range(len(unique))]
        for i in range(len(unique)):
            matrix[i] = [0 for x in range(len(unique))]
        lookup = dict()
        for i, value in enumerate(unique):
            lookup[value] = i
        for i in range(len(self.actual)):
            name = lookup[self.actual[i]]
            key = lookup[self.predicted[i]]
            matrix[name][key] += 1
        return unique, matrix

    def print_confusion_matrix(self):
        """
        Pretty prints a confusion matrix
        :return:
        """

        unique, matrix = self.confusion_matrix()

        print('(P)' + ' '.join(str(val) for val in unique))

        print('(A)---')
        for ind, val in enumerate(unique):
            print("%s| %s" % (val, ' '.join(str(val) for val in matrix[ind])))
        return

    def mae_metric(self):
        """
        Calculate mean absolute error
        :return:
        """
        sum_error = 0.0
        for i in range(len(self.actual)):
            sum_error += abs(self.predicted[i] - self.actual[i])
        return round(sum_error / float(len(self.actual)), 3)

    def rmse_metric(self):
        """
        Calculate root mean squared error
        :return:
        """
        sum_error = 0.0
        for i in range(len(self.actual)):
            prediction_error = self.predicted[i] - self.actual[i]
            sum_error += (prediction_error ** 2)
        mean_error = sum_error / float(len(self.actual))
        return sqrt(mean_error)


if __name__ == "__main__":
    # Test accuracy
    actual = [0, 0, 0, 0, 0, 1, 1, 1, 1, 1]
    predicted = [0, 1, 1, 0, 0, 1, 0, 1, 1, 1]
    accuracy = Accuracy(actual, predicted)

    print(accuracy.accuracy_metric())
    unique, matrix = accuracy.confusion_matrix()
    print(unique)
    print(matrix)

    print(accuracy.print_confusion_matrix())

    # Test RMSE
    actual = [0.1, 0.2, 0.3, 0.4, 0.5]
    predicted = [0.11, 0.19, 0.29, 0.41, 0.5]
    mae = Accuracy(actual, predicted)
    print(mae.mae_metric())


    # Test RMSE
    actual = [0.1, 0.2, 0.3, 0.4, 0.5]
    predicted = [0.11, 0.19, 0.29, 0.41, 0.5]
    rmse = Accuracy(actual, predicted)
    print(rmse.rmse_metric())
