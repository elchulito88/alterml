"""
Example of Making Random Predictions
"""
from random import seed
from random import randrange


class Baseline:
    """
    This class focuses on Baseline formulas
    """

    def __init__(self, train, test):
        self.train = train
        self.test = test

    def random_algorithm(self):
        """
        Generate random predictions
        :return:
        """
        output_values = [row[-1] for row in self.train]
        unique = list(set(output_values))
        predicted = [unique[randrange(len(unique))] for row in self.test]
        return predicted

    def zero_rule_classification(self):
        """
        Zero rule algorithm classification
        :return:
        """
        output_values = [row[-1] for row in self.train]
        prediction = max(set(output_values), key=output_values.count)
        predicted = [prediction for i in range(len(self.test))]
        return predicted

    def zero_rule_regression(self):
        """
        Zero rule algorithm for regression
        :return:
        """
        output_values = [row[-1] for row in self.train]
        prediction = sum(output_values) / float(len(output_values))
        predicted = [prediction for row in range(len(self.test))]
        return predicted


if __name__ == "__main__":
    seed(1)
    TRAIN = [[0], [1], [0], [1], [0], [1]]
    TEST = [[None], [None], [None], [None]]
    PREDICTIONS = Baseline(TRAIN, TEST)
    print(PREDICTIONS.random_algorithm())

    # Zero rule algorithm classification
    TRAIN1 = [['0'], ['0'], ['0'], ['0'], ['1'], ['1']]
    TEST1 = [[None], [None], [None], [None]]
    PREDICTIONS = Baseline(TRAIN1, TEST1)
    print(PREDICTIONS.zero_rule_classification())

    # Zero rule algorithm regression
    TRAIN2 = [[10], [15], [12], [15], [18], [20]]
    TEST2 = [[None], [None], [None], [None]]
    PREDICTIONS = Baseline(TRAIN2, TEST2)
    print(PREDICTIONS.zero_rule_regression())
