"""
This class allows algorithms to be called...

"""

from random import randrange
from random import seed
from math import exp
from eval_methods.accuracy import Accuracy
from eval_methods.train_and_test import TrainTest
from scale_and_standarize.cleansor import Cleaner


class Algorithms:
    """
    This Class is used to call specific Algorithms when necessary
    """

    def __init__(self, dataset, split=None):
        self.dataset = dataset
        self.split = split

    @staticmethod
    def predict(row, coefficients):
        """
        Make a prediction with coefficients
        :param row:
        :param coefficients:
        :return:
        """
        yhat = coefficients[0]
        for i in range(len(row) - 1):
            yhat += coefficients[i + 1] * row[i]
        return yhat

    @staticmethod
    def cross_validation_split(dataset, n_folds):
        """
        Create Cross Validation scores
        :param dataset:
        :param n_folds:
        :return:
        """
        dataset_split = list()
        dataset_copy = list(dataset)
        fold_size = int(len(dataset) / n_folds)
        for i in range(n_folds):
            fold = list()
            while len(fold) < fold_size:
                index = randrange(len(dataset_copy))
                fold.append(dataset_copy.pop(index))
            dataset_split.append(fold)
        return dataset_split

    @staticmethod
    def zero_rule(train, test):
        """
        Create Zero Rule Algorithm Classification
        :param train:
        :param test:
        :return:
        """
        output_values = [row[-1] for row in train]
        prediction = max(set(output_values), key=output_values.count)
        predicted = [prediction for i in range(len(test))]
        return predicted

    @staticmethod
    def mean(values):
        """
        Calculate the mean value of a list of numbers
        :param values:
        :return:
        """
        return sum(values) / float(len(values))

    @staticmethod
    def variance(values, mean):
        """
        Calculate the variance of a list of numbers
        :param values:
        :param mean:
        :return:
        """
        return sum([(x - mean) ** 2 for x in values])

    @staticmethod
    def covariance(x, mean_x, y, mean_y):
        """
        Calculate covariance between x and y
        :param x:
        :param mean_x:
        :param y:
        :param mean_y:
        :return:
        """
        covar = 0.0
        for i in range(len(x)):
            covar += (x[i] - mean_x) * (y[i] - mean_y)
        return covar

    def coefficients(self, dataset):
        """
        Calculate coefficients
        :param dataset:
        :return:
        """
        x = [row[0] for row in dataset]
        y = [row[1] for row in dataset]
        x_mean, y_mean = self.mean(x), self.mean(y)
        b1 = self.covariance(x, x_mean, y, y_mean) / self.variance(x, x_mean)
        b0 = y_mean - b1 * x_mean
        return [b0, b1]

    def coefficients_sgd(self, train, l_rate, n_epoch):
        coef = [0.0 for i in range(len(train[0]))]
        for epoch in range(n_epoch):
            sum_error = 0
            for row in train:
                yhat = self.predict(row, coef)
                error = yhat - row[-1]
                sum_error += error ** 2
                coef[0] = coef[0] - l_rate * error
                for i in range(len(row) - 1):
                    coef[i + 1] = coef[i + 1] - l_rate * error * row[i]
            print('>epoch=%d, lrate=%.3f, error=%.3f' % (epoch, l_rate, sum_error))
        return coef

    def simple_linear_regression(self, train, test):
        """
        Simple linear regression algorithm
        :param train:
        :param test:
        :return:
        """
        predictions = list()
        b0, b1 = self.coefficients(train)
        for row in test:
            yhat = b0 + b1 * row[0]
            predictions.append(yhat)
        return predictions

    def linear_regression_sgd(self, train, test, l_rate, n_epoch):
        """
        Linear Regression Algorithm With Stochastic Gradient Descent
        :param train:
        :param test:
        :param l_rate:
        :param n_epoch:
        :return:
        """
        predictions = list()
        coef = self.coefficients_sgd(train, l_rate, n_epoch)
        for row in test:
            yhat = self.predict(row, coef)
            predictions.append(yhat)
        return predictions

    def evaluate_algorithm(self, algorithm, metric, n_folds=None, *args):
        """
        Evaluate algorithms
        :return:
        """

        if n_folds is not None:
            folds = self.cross_validation_split(self.dataset, n_folds)
            scores = list()

            for fold in folds:
                train_set = list(folds)
                train_set.remove(fold)
                train_set, test_set = sum(train_set, []), [list(row) for row in fold]
                actual, predicted = [row[-1] for row in fold], algorithm(train_set, test_set, *args)
                accuracy = getattr(Accuracy(actual, predicted), metric)
                scores.append(accuracy())

            final_score = [round(x, 4) for x in scores]
            return final_score

        else:
            dataset = TrainTest(self.dataset)
            TrainTest.set_test_percent(self.split)
            train, test = dataset.train_test_split()
            test_set = [list(row) for row in test]
            actual, predicted = [row[-1] for row in test], algorithm(train, test_set, *args)
            accuracy = getattr(Accuracy(actual, predicted), metric)
            rounded_accuracy = round(accuracy(), 4)
            return rounded_accuracy


class LogModel(Algorithms):
    def __init__(self, dataset, split=None):
        super().__init__(dataset, split=None)
        self.dataset = dataset
        self.split = split

    @staticmethod
    def predicts(row, coefficients):
        """
        Make a prediction with coefficients
        :param row:
        :param coefficients:
        :return:
        """
        yhat = coefficients[0]
        for i in range(len(row) - 1):
            yhat += coefficients[i + 1] * row[i]
        return 1.0 / (1.0 + exp(-yhat))

    def coefficients_sgd(self, train, l_rate, n_epoch):
        """
        Estimate logistic regression coefficients using stochastic gradient descent
        :param train:
        :param l_rate:
        :param n_epoch:
        :return:
        """
        coef = [0.0 for _ in range(len(train[0]))]
        for epoch in range(n_epoch):
            sum_error = 0
            for row in train:
                yhat = self.predicts(row, coef)
                error = row[-1] - yhat
                coef[0] = coef[0] + l_rate * error * yhat * (1.0 - yhat)
                for i in range(len(row) - 1):
                    coef[i + 1] = coef[i + 1] + l_rate * error * yhat * (1.0 - yhat) * row[i]
            # print('>epoch=%d, lrate=%.3f, error=%.3f' % (epoch, l_rate, sum_error))
        return coef

    def logistic_regression(self, train, test, l_rate, n_epoch):
        """
        Logistic Regression Algorithm With Stochastic Gradient Descent
        :param train:
        :param test:
        :param l_rate:
        :param n_epoch:
        :return:
        """
        predictions = list()
        coef = self.coefficients_sgd(train, l_rate, n_epoch)
        for row in test:
            yhat = self.predicts(row, coef)
            yhat = round(yhat)
            predictions.append(yhat)
        return predictions


if __name__ == "__main__":
    ## First Example
    seed(1)
    CSV_FILE = Cleaner("../datasets/insurance.csv")
    ACCURACY = Algorithms(CSV_FILE.convert_string_to_float(), .60)
    print(ACCURACY.evaluate_algorithm(ACCURACY.simple_linear_regression, 'rmse_metric'))


    ## Second Example
    # CSV_FILE = Cleaner("D:/alterML/datasets/winequality-white.csv")
    # CSV_FILE.convert_string_to_float()
    # CSV_FILE.dataset_minmax()
    # ACCURACY = Algorithms(CSV_FILE.normalize_dataset())
    #
    # n_folds, l_rate, n_epoch = 5, 0.01, 50
    # model = ACCURACY.linear_regression_sgd
    #
    # scores = ACCURACY.evaluate_algorithm(model, 'rmse_metric', n_folds, l_rate, n_epoch)
    #
    # print(scores)
    # print('Mean RMSE: %.3f' % (sum(scores) / float(len(scores))))

    # Calculate coefficients
    # dataset = [[1, 1], [2, 3], [4, 3], [3, 2], [5, 5]]
    # l_rate = 0.001
    # n_epoch = 50
    # A = Algorithms(dataset)
    # coef = A.coefficients_sgd(dataset, l_rate, n_epoch)
    # print(coef)

    # Logistic Examples...
    # seed(1)
    # dataset = [[2.7810836, 2.550537003, 0],
    #            [1.465489372, 2.362125076, 0],
    #            [3.396561688, 4.400293529, 0],
    #            [1.38807019, 1.850220317, 0],
    #            [3.06407232, 3.005305973, 0],
    #            [7.627531214, 2.759262235, 1],
    #            [5.332441248, 2.088626775, 1],
    #            [6.922596716, 1.77106367, 1],
    #            [8.675418651, -0.242068655, 1],
    #            [7.673756466, 3.508563011, 1]]
    #
    # log = logModel(dataset)
    # l_rate = 0.3
    # n_epoch = 100
    # coef = log.coefficients_sgd(dataset, l_rate, n_epoch)
    # print(coef)


    ## Final Example
    # Look at page 78
    # seed(1)
    # CSV_FILE = Cleaner("/Volumes/ALTERML/alterML/datasets/pima-indians-diabetes.csv")
    # folds = 5
    # rate = 0.1
    # epoch = 100
    # split = None
    # logScore = LogModel(CSV_FILE.normalize_dataset(), split)
    #
    # scores = logScore.evaluate_algorithm(logScore.logistic_regression, 'accuracy_metric', folds, rate, epoch)
    # print('Mean Accuracy: %.3f%%' % (sum(scores) / float(len(scores))))
    # # 'rmse_metric'
