# README #


### This is an Open Source Machine Learning Library based on notes from Jason Brownlee's ML books

* Version 1.0.0

### How do I get set up? ###

* Configuration - pip install -r requirements.txt
* How to run tests
    * Check out the test_results dir

### Who do I talk to? ###

* John Posada
* johnposada88@gmail.com
