from formulas.evaluate_algorithm import LogModel
from random import seed
from scale_and_standarize.cleansor import Cleaner
import os
import unittest


class TestAlgorithm(unittest.TestCase):

    def test_evaluate_algorithm(self):
        list_validator = "Mean Accuracy: 77.124%"
        data_path = os.path.abspath("./datasets/pima-indians-diabetes.csv")
        seed(1)
        csv_file = Cleaner(data_path)
        folds = 5
        rate = 0.1
        epoch = 100
        split = None
        logscore = LogModel(csv_file.normalize_dataset(), split)
        scores = logscore.evaluate_algorithm(logscore.logistic_regression, 'accuracy_metric', folds, rate, epoch)
        validator = 'Mean Accuracy: %.3f%%' % (sum(scores) / float(len(scores)))
        self.assertEqual(validator, list_validator)


if __name__ == '__main__':
    unittest.main()

