from parsing.text_scraper import FileScrapper
import unittest
from subprocess import Popen, PIPE
import os
import ast

val_one = [[437, 'objective'], [439, 'objective'], [441, 'objective']]


script_path = os.path.abspath('./parsing/text_scraper.py')
text_path = os.path.abspath('./datasets/final.txt')
key_word = 'objective'

# print(script_path, text_path)
# print(os.path.exists(script_path))
# print(os.path.exists(text_path))


# # Set up the echo command and direct the output to a pipe
# p = Popen(["python", script_path, '-f', '%s' % text_path, '-s', '%s' % key_word], stdin=PIPE, stdout=PIPE, stderr=PIPE)
# output, err = p.communicate(b"input data that is passed to subprocess' stdin")
# rc = p.returncode
#
# print(ast.literal_eval(output.decode()) == val_one)


class TestScraper(unittest.TestCase):

    def test_load_csv(self):
        scraper = FileScrapper(text_path, key_word)
        self.assertEqual(scraper.load_csv(), val_one)


if __name__ == '__main__':
    unittest.main()

