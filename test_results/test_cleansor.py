from formulas.evaluate_algorithm import Algorithms
from random import seed
from scale_and_standarize.cleansor import Cleaner
import unittest


class TestCleansor(unittest.TestCase):
    def test_evaluate_algorithm(self):
        seed(1)
        CSV_FILE = Cleaner("./datasets/insurance.csv")
        ACCURACY = Algorithms(CSV_FILE.convert_string_to_float(), .60)
        self.assertEqual(ACCURACY.evaluate_algorithm(ACCURACY.simple_linear_regression, 'rmse_metric'), 33.6298)


if __name__ == '__main__':
    unittest.main()
