from parsing.text_scraper import FileScrapper
import os
import unittest



class TestScraper(unittest.TestCase):

    def test_load_csv(self):
        list_validator = [[437, 'objective'], [439, 'objective'], [441, 'objective']]
        text_path = os.path.abspath('./datasets/final.txt')
        key_word = 'objective'
        scraper = FileScrapper(text_path, key_word)
        self.assertEqual(scraper.load_csv(), list_validator)

if __name__ == '__main__':
    unittest.main()

