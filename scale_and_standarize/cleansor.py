"""
Creating a class for reading csv files and determining the min and max values.
Scaling and Standardization is next.
"""
from csv import reader
from math import sqrt


class Cleaner:
    def __init__(self, f_name, col_num=None):
        self.filename = f_name
        self.column = col_num

    def load_csv(self):
        dataset = list()
        with open(self.filename, 'r') as file:
            csv_reader = reader(file)
            for row in csv_reader:
                if not row:
                    continue
                dataset.append(row)
        return dataset

    def convert_string_to_float(self):
        """
        Convert string columns to float
        :param self:
        :return:
        """
        dataset = self.load_csv()

        def str_column_to_float(data, column):
            """
            Convert string column to float
            :param column:
            :return:
            """
            for row in data:
                row[column] = float(row[column].strip())

        for i in range(len(dataset[0])):
            str_column_to_float(dataset, i)

        return dataset

    def str_column_to_int(self):
        """
        Convert string column to integer
        :return:
        """
        data = self.load_csv()
        column = self.column
        class_values = [row[column] for row in data]
        unique = set(class_values)
        lookup = dict()
        for i, value in enumerate(unique):
            lookup[value] = i
        for row in data:
            row[column] = lookup[row[column]]
        return lookup

    def dataset_minmax(self):
        """
        Finds the min and max values
        :return:
        """
        dataset = self.convert_string_to_float()
        minmax = list()
        for i in range(len(dataset[0])):
            col_values = [row[i] for row in dataset]
            value_min = min(col_values)
            value_max = max(col_values)
            minmax.append([value_min, value_max])
        return minmax

    def normalize_dataset(self):
        """
        Rescale dataset columns to the range 0-1
        :return:
        """
        dataset = self.convert_string_to_float()
        minmax = self.dataset_minmax()
        for row in dataset:
            for i in range(len(row)):
                row[i] = (row[i] - minmax[i][0]) / (minmax[i][1] - minmax[i][0])
        return dataset

    def column_means(self):
        """
        Calculate the mean value
        :return:
        """
        dataset = self.convert_string_to_float()
        means = [0 for i in range(len(dataset[0]))]
        for i in range(len(dataset[0])):
            col_values = [row[i] for row in dataset]
            means[i] = sum(col_values) / float(len(dataset))
        return means

    def column_stdevs(self):
        """
        Calculate the standard deviation
        :param means:
        :return:
        """
        means = self.column_means()
        dataset = self.convert_string_to_float()
        stdevs = [0 for i in range(len(dataset[0]))]
        for i in range(len(dataset[0])):
            variance = [pow(row[i] - means[i], 2) for row in dataset]
            stdevs[i] = sum(variance)
        stdevs = [sqrt(x / (float(len(dataset) - 1))) for x in stdevs]
        return stdevs

    def standardize_dataset(self):
        """
        Standarize the dataset
        :param means:
        :param stdevs:
        :return:
        """
        dataset = self.convert_string_to_float()
        means = self.column_means()
        stdevs = self.column_stdevs()
        for row in dataset:
            for i in range(len(row)):
                row[i] = (row[i] - means[i]) / stdevs[i]

        return dataset
